rectMode(CENTER);
smooth();
for (int i=0; i<100; i= i+6) {
  pushMatrix();
  translate(i, 50);
  float angle = map(i, 0, 100, 0, TWO_PI);
  rotate(angle);
  rect(0, 0, 5, 5);
  popMatrix();
}

