color pick;

void setup() {
  background(255);
  size(600, 300);
  // noStroke();
  smooth();
}

void draw() {
  noStroke();  

  for (int i=0; i<width/2; i+=5) {
    for (int j=0; j<height; j+=5) {
      float R = j*1.4;
      float G = i*0.8;
      float B = j*0.5; 
      color c = color(R, G, B);
      fill(c);

      rect(i, j, 5, 5);
    }
  }
  fill(pick);
  rect(0, 0, 300, 10);
  
  if (mousePressed) {
    if (mouseX<width/2)pick = get(mouseX, mouseY);
    if (mouseX>width/2) {
      stroke(pick);
      point(mouseX, mouseY);
    }
  }
}

