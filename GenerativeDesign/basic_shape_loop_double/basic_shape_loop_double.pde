void draw() {
  background(255);
  rectMode(CENTER);
  smooth();
  for (int i=5; i<100; i= i+10) {
    for (int j=5; j<100; j+= 10) {
      pushMatrix();
      translate(i, j);
      float angle = map(i, 0, 100, 0, TWO_PI);
      rotate(angle);
      rect(0, 0, 5, 5);
      popMatrix();
    }
  }
}

