PImage speak;
PImage [] buffer;

int ratio = 15, widthRatio, heightRatio;
float noiseScale = 0;
int choix, noiseInt;

void setup() {
  size(800, 600);
  background(255);
  smooth();

  speak = loadImage("flowers.JPG");
  speak.resize(400, 400);
  noiseSeed(2);

  buffer = new PImage[ratio * ratio];
  widthRatio = speak.width / ratio;
  heightRatio = speak.height / ratio;

  loadPixels();

  buffer = splitImage(speak, ratio);

  for (int i=0; i<width; i+=widthRatio) {
    for (int j=0; j<height; j+=heightRatio) {
        //image(buffer[(int)(random(buffer.length))], i, j);//------------------ random version
      for (choix=noiseInt; choix<buffer.length; choix+=noiseInt) {
        noiseScale-=.01;
        float noiseVal = noise(noiseScale) ;
        noiseInt = (int)map(noiseVal, 0, 1, 0, buffer.length);
        pushMatrix();
        translate(i+(buffer[choix].width/4), j+(buffer[choix].height/4));
        scale(0.8);
        //image(buffer[choix], 0, 0);//-------------------------------------- noise version
        popMatrix();
      }
    }
  }
}

PImage [] splitImage(PImage splitMe, int ratio) {
  PImage [] buffSplit = new PImage[ratio * ratio];

  for (int i = 0; i < buffSplit.length; i++) {
    buffSplit[i] = new PImage(widthRatio, heightRatio);
    buffSplit[i].copy(splitMe, 
    ((i * widthRatio) % splitMe.width), 
    ((i / (splitMe.height / heightRatio)) * heightRatio), 
    widthRatio, 
    heightRatio, 
    0, 
    0, 
    buffSplit[i].width, 
    buffSplit[i].height);
  }
  return buffSplit;
}

void draw() {
}

void keyPressed() {
  if (key == 's' || key == 'S') {
    saveFrame(timestamp()+"_##.png");
  }
}

String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

