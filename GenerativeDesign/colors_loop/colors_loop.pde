void setup() {
  size(300,300);
  noStroke();
}

void draw() {

  for (int i=0; i<width; i+=5) {
    for (int j=0; j<height; j+=5) {
      int R = j*2;
      float G = i*1.5;
      float B = j*0.5; 
      color c = color(R, G, B);
      fill(c);
      rect(i, j, 5, 5);
    }
  }
}

