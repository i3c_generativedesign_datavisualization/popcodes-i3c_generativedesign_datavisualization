import processing.opengl.*;

float nX = 0.0;
float nY = 0.0;
float nZ = 0.0;
float nW = 0.0;
float nH = 0.0;

void setup() {
  size(600, 600, OPENGL);
  hint(ENABLE_OPENGL_4X_SMOOTH);
}

void draw() {
  background(255);
  for (int i=150; i<450; i+=24) {
    for (int j=450; j>150; j-=25) {
      nX += 0.000012;
      nY += 0.000017;
      nZ += 0.000011;
      nW += 0.000016;
      nH += 0.000018;

      float _nX = noise(nX) * 10;
      float _nY = noise(nY) * 10;
      float _nW = noise(nW) * 300;
      float _nH = noise(nH) * 300;
      float _nZ = noise(nZ) * 10;
      
      pushMatrix();
      translate(i, j);

      rotateX(_nX);
      rotateY(_nY);
      rotateZ(_nZ);
      line(_nW, i, j, _nH);
      // ellipse(0, 0, 10, 10);
      popMatrix();
      //      line(_nX, j, i, _nY);
      // line(_nX, _nY, i, j);
      // line(i, j, _nX, _nY);
    }
  }
}

void keyPressed() {
  if (key == 's' || key == 'S') {
    saveFrame(timestamp()+"_##.png");
  }
}

String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

