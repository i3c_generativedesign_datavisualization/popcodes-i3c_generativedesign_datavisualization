float nX = 0.0;
float nY = 0.0;

void setup() {
  size(300, 300);
  smooth();
}

void draw() {
  background(255);
  for (int i=0; i<width; i+=15) {
    for (int j=height; j>0; j-=17) {
      nX += 0.00012;
      nY += 0.00017;
      float _nX = noise(nX) * width;
      float _nY = noise(nY) * height;
      //   line(i, height/2, _n, j);
      line(i, j, _nX, _nY);
    }
  }
}

