String myText[] = loadStrings("/home/algo/sketchbook/Pedago/Séminaire_I3C_generativeDesign_visualizingData/vizualisingData/simple_text_analysis_007/data/chomsky.txt");
String [] words;

float []radian;
float []total;

float [] angle;

float [] posX; // position of the circles
float [] posY; //

void setup() {
  size(800, 600);
  background(255);
  smooth();
  fill(0);

  for (int i=0; i<myText.length; i++) {
    words = split(myText[i], ' ');            // split my sentences into words with split()

    //if(myText[i].equals(" ")) println("espace");
    int nb = myText.length;                   // number of sentences

    posX = new float[nb];
    posY = new float[nb];
    radian = new float[nb];
    total = new float[nb];
    angle = new float[nb];
    
    radian[i] = random(30, 200);

    posX[i] = radian[i]/2 + i*(width/myText.length);
    posY[i] = radian[i]/2 + i*(height/myText.length);



    total[i] = 360f / words.length;            // divided by number of words - the 'f' behind 36à is important to do a float operation
    angle[i] = radians(total[i]);             // same angle between 2 words

    float count = 0;
    fill(#7A7FB9);
    noStroke();
    ellipse(posX[i], posY[i], radian[i], radian[i]);

    for (int j=0; j<words.length; j++) {
      fill(0, 75);
      count += angle[i];

      float x = posX[i] + cos(count) * radian[i]/2;
      float y = posY[i] + sin(count) * radian[i]/2;

      if (j==0)fill(#1777E8);
      else fill(0, 125);

      if (words[j].equals("courageous") || words[j].equals("honorable"))fill(#F7A114);
      pushMatrix();
      translate(x, y);

      rotate(count + TWO_PI );


      text(words[j], 0, 0);

      popMatrix();

      println(words.length+"   " +total[i]+"   "+angle[i]+"      __"+x+"     count  "+count+"   radian = " + radian[i]);
    }

    //text(i, posX[i], posY[i]);
  }
}

