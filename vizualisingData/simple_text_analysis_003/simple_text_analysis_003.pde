String myText[] = loadStrings("/home/algo/sketchbook/Pedago/Séminaire_I3C_generativeDesign_visualizingData/vizualisingData/simple_text_analysis/data/chomsky.txt");
String [] words;
float x = 0, y = 10;
float angle = 0.0;
float radius = 25;

void setup() {
  size(800, 600);
  translate(width/6, height/2);
  for (int i=0; i<myText.length; i++) {
    words = split(myText[i], ' ');
    for (int j=0; j<words.length; j++) {
      angle += 0.1;

      pushMatrix();
      x = x + sin(angle) * radius;
      y = y + cos(angle) * radius;
      translate(x, y);
      text(words[j], 0, 0);
      popMatrix();
    }
  }
}

