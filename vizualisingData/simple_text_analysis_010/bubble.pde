class Bubble {
  PVector location;
  PVector velocity;
  float dim;
  String myWord;
  color c;
  float d, _shift;
  float minRadius;
  ArrayList others;
  int id;

  Bubble(String _myWord, float _x, float _y, int _id, ArrayList _list) {
    myWord = _myWord;
    location = new PVector(_x, _y);
    velocity = new PVector(random(-0.5, 0.5), random(-0.5, 0.5));


    dim = textWidth(myWord) + (textWidth(myWord))/3; 
    d = textDescent();
    _shift = dim/26;      // some adjustements to center the word into the circle.
    minRadius = dim / 4;
    id =_id;
    others = _list;
  }

  void display() {
    //fill(c, 25);
    noFill();

    ellipse( location.x, location.y - d, dim, dim);
    fill(c);
    text(myWord, location.x- _shift, location.y);
  }

  void update() {
    location.add(velocity);
  }

  void checkEdges() {

    if (location.x > width) {
      location.x = 0;
    } 
    else if (location.x < 0) {
      location.x = width;
    }

    if (location.y > height) {
      location.y = 0;
    } 
    else if (location.y < 0) {
      location.y = height;
    }
  }
  // from the bouncybubbles example

  void collide() {
    for (int i = id + 1; i < nb; i++) {
      Bubble b = (Bubble) others.get(i);

      float dx = b.location.x - location.x;
      float dy = b.location.y - location.y;
      float distance = sqrt(dx*dx + dy*dy);    // distance between 2 circles coordinates
      float minDist = b.dim/2 + dim/2;
      if (distance < minDist) { 
        float angle = atan2(dy, dx);
        float targetX = location.x + cos(angle) * minDist;
        float targetY = location.y + sin(angle) * minDist;
        float ax = (targetX - b.location.x) * spring;
        float ay = (targetY - b.location.y) * spring;
        velocity.x -= ax;
        velocity.y -= ay;
        b.velocity.x += ax;
        b.velocity.y += ay;
      }
    }
  }
}

