String myText[] = loadStrings("/home/algo/sketchbook/Pedago/Séminaire_I3C_generativeDesign_visualizingData/vizualisingData/simple_text_analysis_8/data/chomsky.txt");
String [] words;
int nb = 0;
ArrayList myBubble;          // a dynamic array contained my bubble
float spring = 0.001;


void setup() {
  size(800, 600);
  background(255);
  smooth();
  fill(0);
  textAlign(CENTER);
  ellipseMode(CENTER);

  myBubble = new ArrayList();

  for (int i=0; i<myText.length; i++) {
    words = splitTokens(myText[i], " ––.");            // split my sentences into words with split()
    for (int j=0; j<words.length; j++) {
      myBubble.add(new Bubble(words[j], random(width), random(height), i, myBubble));
      nb++;
    }
  }
}

void draw() {
  background(255);
  for (int i=0; i<myBubble.size(); i++) {
    Bubble bubble = (Bubble) myBubble.get(i);
    bubble.display();
    bubble.update();
    bubble.checkEdges();
    bubble.collide();          // check the other bubble
  }
}

