String myText[] = loadStrings("/home/algo/sketchbook/Pedago/Séminaire_I3C_generativeDesign_visualizingData/vizualisingData/simple_text_analysis/data/chomsky.txt");
String [] words;
float x = 0, y = 10;

void setup() {
  size(800, 300);
  for (int i=0; i<myText.length; i++) {
    words = split(myText[i], ' ');
    for (int j=0; j<words.length; j++) {
      char[] letters = words[j].toCharArray();

      y+=10;
      if (y>=height) {
        y=10;
        x+=100;
      }
      text(words[j], x, y);
    }
  }
}

