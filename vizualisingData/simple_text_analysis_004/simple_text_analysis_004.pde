String myText[] = loadStrings("/home/algo/sketchbook/Pedago/Séminaire_I3C_generativeDesign_visualizingData/vizualisingData/simple_text_analysis_5/data/chomsky.txt");
String [] words;

float []radian;
float []total;

float [] angle;

float [] posX; // position of the circles
float posY[];

void setup() {
  size(800, 600);
  background(255);
  fill(0);

  for (int i=0; i<myText.length; i++) {
    words = split(myText[i], ' ');            // split my sentences into words with split()
    int nb = myText.length;                   // number of sentences

    posX = new float[nb];
    posY = new float[nb];
    radian = new float[nb];
    total = new float[nb];
    angle = new float[nb];

    posX[i] = random(50, width-50);
    posY[i] = i*(height/myText.length);//random(50, height-50);
    radian[i] = random(30, 100);

    total[i] = 360 / words.length;            // divided by number of words
    angle[i] = radians(total[i]);             // same angle between 2 words

    float count = 0;

    for (int j=0; j<words.length; j++) {

      count += angle[i];

      float x = posX[i] + cos(count) * radian[i];
      float y = posY[i] + sin(count) * radian[i];

      if (j==0)fill(#1777E8);
      else fill(0, 125);
      

      pushMatrix();
      translate(x, y);

      rotate(count + TWO_PI );


      text(words[j], 0, 0);

      popMatrix();

      println(words.length+"   " +total[i]+"   "+angle[i]+"      __"+x+"     count  "+count+"   radian = " + radian[i]);
    }
    fill(0, 75);
    text(i, posX[i], posY[i]);
  }
}

