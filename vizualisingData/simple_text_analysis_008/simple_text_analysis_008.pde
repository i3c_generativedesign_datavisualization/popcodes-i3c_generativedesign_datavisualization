String myText[] = loadStrings("/home/algo/sketchbook/Pedago/Séminaire_I3C_generativeDesign_visualizingData/vizualisingData/simple_text_analysis_8/data/chomsky.txt");
String [] words;

void setup() {
  size(800, 600);
  background(255);
  smooth();
  fill(0);
  textAlign(CENTER);
  ellipseMode(CENTER);

  for (int i=0; i<myText.length; i++) {
    words = split(myText[i], ' ');            // split my sentences into words with split()

    for (int j=0; j<words.length; j++) {
      Bubble bubble[] = new Bubble[words.length];
     
      bubble[j] = new Bubble(words[j], random(width), random(height), textWidth(words[j]));
      bubble[j].display();
    }
  }
}

