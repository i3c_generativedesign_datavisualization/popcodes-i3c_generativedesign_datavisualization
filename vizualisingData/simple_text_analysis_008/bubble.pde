class Bubble {
  float x, y, dim;
  String myWord;
  color c;

  Bubble(String _myWord, float _x, float _y, float _dim) {
    myWord = _myWord;
    x = _x;
    y = _y;
    dim = _dim + _dim/3;
  }

  void display() {
    fill(c, 25);
    float d = textDescent();
    float _shift = dim/26;      // some adjustements to center the word into the circle.
    ellipse( x, y - d, dim, dim);
    fill(c);
    text(myWord, x- _shift, y);
  }
}

