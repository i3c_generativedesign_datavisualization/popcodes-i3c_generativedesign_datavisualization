String myText[] = loadStrings("/home/algo/sketchbook/Pedago/Séminaire_I3C_generativeDesign_visualizingData/vizualisingData/simple_text_analysis_8/data/chomsky.txt");
String [] words;

ArrayList myBubble;          // a dynamic array contained my bubble


void setup() {
  size(800, 600);
  background(255);
  smooth();
  fill(0);
  textAlign(CENTER);
  ellipseMode(CENTER);

  myBubble = new ArrayList();

  for (int i=0; i<myText.length; i++) {
    words = split(myText[i], ' ');            // split my sentences into words with split()
    for (int j=0; j<words.length; j++) {
      myBubble.add(new Bubble(words[j], random(width), random(height), textWidth(words[j])));
    }
  }
}

void draw() {
  background(255);
  for (int i=0; i<myBubble.size(); i++) {
    Bubble bubble = (Bubble) myBubble.get(i);
    bubble.display();
    bubble.update();
    bubble.checkEdges();
  }
}

