class Bubble {
  PVector location;
  PVector velocity;
  float dim;
  String myWord;
  color c;
  float d, _shift;

  Bubble(String _myWord, float _x, float _y, float _dim) {
    myWord = _myWord;
    location = new PVector(_x, _y);
    velocity = new PVector(random(-0.5, 0.5), random(-0.5, 0.5));
    dim = _dim + _dim/3;
    d = textDescent();
    _shift = dim/26;      // some adjustements to center the word into the circle.
  }

  void display() {
    //fill(c, 25);
    noFill();

    ellipse( location.x, location.y - d, dim, dim);
    fill(c);
    text(myWord, location.x- _shift, location.y);
  }

  void update() {
    location.add(velocity);
  }

  void checkEdges() {

    if (location.x > width) {
      location.x = 0;
    } 
    else if (location.x < 0) {
      location.x = width;
    }

    if (location.y > height) {
      location.y = 0;
    } 
    else if (location.y < 0) {
      location.y = height;
    }
  }
}

